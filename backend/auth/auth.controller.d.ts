import { AuthLoginDto } from './auth-login.dto';
import { AuthService } from './auth.service';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    login(authLoginDto: AuthLoginDto): Promise<{
        access_token: string;
    }>;
    test(): Promise<string>;
}
