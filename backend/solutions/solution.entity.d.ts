import { BaseEntity } from "typeorm";
export declare class Solution extends BaseEntity {
    id: number;
    task: number;
    user: number;
    url: string;
    fileName: string;
    createdAt: Date;
}
