import { CreateSoltionDto } from './create-solution.dto';
import { SolutionsService } from './solutions.service';
export declare class SolutionsController {
    private readonly solutionsService;
    constructor(solutionsService: SolutionsService);
    create(CreatecolutionDto: CreateSoltionDto): Promise<import("./solution.entity").Solution>;
    showSolutionOfOne(id: string): Promise<import("./solution.entity").Solution[]>;
}
