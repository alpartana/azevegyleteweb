import { CreateSoltionDto } from './create-solution.dto';
import { Solution } from './solution.entity';
export declare class SolutionsService {
    create(createSolutionDto: CreateSoltionDto): Promise<Solution>;
    showSolutions(Id: string): Promise<Solution[]>;
}
