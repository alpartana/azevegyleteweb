"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SolutionsService = void 0;
const common_1 = require("@nestjs/common");
const solution_entity_1 = require("./solution.entity");
let SolutionsService = class SolutionsService {
    async create(createSolutionDto) {
        const solution = solution_entity_1.Solution.create(createSolutionDto);
        await solution.save();
        return solution;
    }
    async showSolutions(Id) {
        console.log("Service:" + Id);
        const id = parseInt(Id);
        console.log(id);
        const solutions = solution_entity_1.Solution.find({ where: { task: id } });
        return solutions;
    }
};
SolutionsService = __decorate([
    (0, common_1.Injectable)()
], SolutionsService);
exports.SolutionsService = SolutionsService;
//# sourceMappingURL=solutions.service.js.map