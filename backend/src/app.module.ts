import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TasksModule } from './tasks/tasks.module';
import { SolutionsService } from './solutions/solutions.service';
import { SolutionsModule } from './solutions/solutions.module';

@Module({
  imports: [TypeOrmModule.forRoot(), UsersModule, AuthModule, TasksModule, SolutionsModule],
  controllers: [AppController],
  providers: [AppService, SolutionsService],
})
export class AppModule {}
