import { IsNotEmpty } from "class-validator";

export class CreateSoltionDto {
    @IsNotEmpty()
    task: number

    @IsNotEmpty()
    user: number

    url: string

    fileName: string
}