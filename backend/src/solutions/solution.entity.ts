import { Task } from "src/tasks/task.entity";
import { BaseEntity, Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Solution extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    task: number

    @Column()
    user: number

    @Column()
    url: string

    @Column()
    fileName: string

    @Column()
    @CreateDateColumn()
    createdAt: Date;
}