import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateSoltionDto } from './create-solution.dto';
import { SolutionsService } from './solutions.service';

@Controller('solutions')
export class SolutionsController {
  constructor(private readonly solutionsService: SolutionsService) {}
  
  @Post()
  @UseGuards(JwtAuthGuard)
  create(@Body() CreatecolutionDto: CreateSoltionDto){
    return this.solutionsService.create(CreatecolutionDto)
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  showSolutionOfOne(@Param('id') id:string){
    console.log("controller:" + id);
    return this.solutionsService.showSolutions(id);
  }
}
