import { Injectable } from '@nestjs/common';
import { CreateSoltionDto } from './create-solution.dto';
import { Solution } from './solution.entity';

@Injectable()
export class SolutionsService {
    async create(createSolutionDto: CreateSoltionDto){
        const solution = Solution.create(createSolutionDto);
        await solution.save();

        return solution;
    }


    async showSolutions(Id: string){
        console.log("Service:" + Id);
        const id:number = parseInt(Id);
        console.log(id);
        const solutions = Solution.find({where: {task: id}});
        return solutions;
    }
}
