import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateTaskDto } from './create-task.dto';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  create(@Body() CreateTaskDto: CreateTaskDto){
    return this.tasksService.create(CreateTaskDto);
  }

  @Get()
  //@UseGuards(JwtAuthGuard)
  showAll() {
    return this.tasksService.showAll();
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  showOne(@Param(':id') id:string) {
    return this.tasksService.findById(+id);
  }
}
