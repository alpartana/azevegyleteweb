import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './create-task.dto';
import { Task } from './task.entity';

@Injectable()
export class TasksService {
    async create(createTaskDto: CreateTaskDto){
        const task = Task.create(createTaskDto);
        await task.save();

        return task;
    }

    async showById(Id: number): Promise<Task> {
        const task = await this.findById(Id);
        return task;
    }

    async showAll(): Promise<Task[]> {
        const tasks = await Task.find()
        return tasks;
    }

    async findById(Id: number) {
        return await Task.findOne(Id);
    }

    async getSolutions(Id: number) {
        const task = await this.findById(Id);
        const solutions = task.solutions;
        return solutions;
    }

}
