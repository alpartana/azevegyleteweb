import { IsEmail, IsNotEmpty } from "class-validator"

export class CreateUserDto {
    @IsEmail()
    email: string

    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    passwod: string

    @IsNotEmpty()
    contactName: string;

    @IsNotEmpty()
    contactTelephoneNumber: string;
}