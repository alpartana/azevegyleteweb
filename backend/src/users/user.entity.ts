import * as bcrypt from 'bcryptjs';
import { Solution } from 'src/solutions/solution.entity';
import { BaseEntity, BeforeInsert, Column, CreateDateColumn, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class User extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique: true})
    email: string;

    @Column()
    name: string

    @Column()
    password: string;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date

    @Column()
    contactName: string;

    @Column()
    contactTelephoneNumber: string;

    @OneToMany(type => Solution, solution => solution.user)
    @JoinColumn({referencedColumnName: "solutions"})
    solutions: Solution[];

    @BeforeInsert()
    async hashPassword(){
        this.password = await bcrypt.hash(this.password, 8);
    }

    async validatePassword(password:string): Promise<boolean> {
        return bcrypt.compare(password, this.password);
    }
}