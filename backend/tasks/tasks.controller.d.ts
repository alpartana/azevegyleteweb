import { CreateTaskDto } from './create-task.dto';
import { TasksService } from './tasks.service';
export declare class TasksController {
    private readonly tasksService;
    constructor(tasksService: TasksService);
    create(CreateTaskDto: CreateTaskDto): Promise<import("./task.entity").Task>;
    showAll(): Promise<import("./task.entity").Task[]>;
    showOne(id: string): Promise<import("./task.entity").Task>;
}
