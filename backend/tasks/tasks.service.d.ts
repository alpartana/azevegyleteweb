import { CreateTaskDto } from './create-task.dto';
import { Task } from './task.entity';
export declare class TasksService {
    create(createTaskDto: CreateTaskDto): Promise<Task>;
    showById(Id: number): Promise<Task>;
    showAll(): Promise<Task[]>;
    findById(Id: number): Promise<Task>;
    getSolutions(Id: number): Promise<import("../solutions/solution.entity").Solution[]>;
}
