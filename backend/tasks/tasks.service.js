"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TasksService = void 0;
const common_1 = require("@nestjs/common");
const task_entity_1 = require("./task.entity");
let TasksService = class TasksService {
    async create(createTaskDto) {
        const task = task_entity_1.Task.create(createTaskDto);
        await task.save();
        return task;
    }
    async showById(Id) {
        const task = await this.findById(Id);
        return task;
    }
    async showAll() {
        const tasks = await task_entity_1.Task.find();
        return tasks;
    }
    async findById(Id) {
        return await task_entity_1.Task.findOne(Id);
    }
    async getSolutions(Id) {
        const task = await this.findById(Id);
        const solutions = task.solutions;
        return solutions;
    }
};
TasksService = __decorate([
    (0, common_1.Injectable)()
], TasksService);
exports.TasksService = TasksService;
//# sourceMappingURL=tasks.service.js.map