import { Solution } from 'src/solutions/solution.entity';
import { BaseEntity } from 'typeorm';
export declare class User extends BaseEntity {
    id: number;
    email: string;
    name: string;
    password: string;
    createdAt: Date;
    updatedAt: Date;
    contactName: string;
    contactTelephoneNumber: string;
    solutions: Solution[];
    hashPassword(): Promise<void>;
    validatePassword(password: string): Promise<boolean>;
}
