import { CreateUserDto } from './create-user.dto';
import { User } from './user.entity';
export declare class UsersService {
    create(createUserDto: CreateUserDto): Promise<User>;
    showById(id: number): Promise<User>;
    findById(id: number): Promise<User>;
    findByEmail(email: string): Promise<User>;
}
