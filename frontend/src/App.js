import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import MainPage from "./components/Pages/MainPage";

function App() {
  return (
    <div
      style={{
        backgroundColor: "#503D2E",
        color: "white",
        height: "100%",
      }}
    >
      <MainPage></MainPage>
    </div>
  );
}

export default App;
