import React, { useState } from "react";

import Input from "../UI/Input";
import Button from "../UI/Button";
import Text from "../UI/Text";
import RegisterForm from "./RegisterForm";

const LoginForm = (props) => {
  const [loginFailMessage, setLoginFailMessage] = useState("");
  const [loginData, setLoginData] = useState({
    email: "",
    password: "",
  });

  // sets the email, and the password in the state
  const handleChange = (event, stateName) => {
    setLoginData((prevState) => {
      return {
        ...prevState,
        [stateName]: event.target.value,
      };
    });
  };

  // TODO: onSubmit

  return (
    <div className="sidebar">
      <Text>Bejelentkezés</Text>
      <Input
        type="email"
        onChange={(event) => handleChange(event, "email")}
        value={loginData.email}
        placeholder="E-mail"
      />

      <Input
        type="password"
        onChange={(event) => handleChange(event, "password")}
        value={loginData.password}
        placeholder="Jelszó"
      />

      <Text className="login-form__warning-message">{loginFailMessage}</Text>

      <Button onClick={(event) => onsubmit(event)}>Bejelentkezés</Button>
    </div>
  );
};

export default LoginForm;
