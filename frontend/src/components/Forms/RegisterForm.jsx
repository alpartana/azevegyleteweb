import React, {useState, useEffect} from "react";
import validator from "validator";

import Input from "../UI/Input";
import Button from "../UI/Button";
import Text from "../UI/Text";
import CheckBox from "../UI/CheckBox";

const RegisterForm = () => {
  const [signUpData, setSignUpData] = useState({
    teamName: "",
    location: "",
    contact: "",
    email: "",
    phoneNumber: "",
    password: "",
    repeatedPassword: "",
    agreed: false
  });

  const [currentField, setCurrentField] = useState("");
  const [errors, setErrors] = useState({
    email: "",
    phoneNumber: "",
    password: "",
    repeatedPassword: "",
    agreed: ""
  });

  useEffect(() => {
    validation();
  }, [signUpData]);

  const handleChange = (event, stateName) => {
    setCurrentField(stateName);
    setSignUpData((prevState) => {
      return {
        ...prevState,
        [stateName]: event.target.value
      };
    });
  };

  const handleChangeCheckbox = (event, stateName) => {
    setSignUpData((prevState) => {
      return {
        ...prevState,
        [stateName]: event.target.checked ? true : false,
      };
    });
  }

  const errorUpdate = (errorField, errorMessage) => {
    setErrors((prevState) => {
      return {
        ...prevState,
        [errorField]: errorMessage
      };
    });
  };

  const validation = () => {
    switch (currentField) {
      case "email": {
        validator.isEmail(signUpData.email) ?
          errorUpdate("email", "") :
          errorUpdate("email", "Hibás e-mail cím");

        break;
      }

      case "phoneNumber": {
        validator.isMobilePhone(signUpData.phoneNumber, "ro-RO") ?
          errorUpdate("phoneNumber", "") :
          errorUpdate("phoneNumber", "Hibás telefonszám");

        break;
      }

      case "password": {
        validator.isStrongPassword(signUpData.password, {minSymbols: 0}) ? 
          errorUpdate("password", "") : 
          errorUpdate("password", "Hibás jelszó, tartalmaznia kell nagy és kisbetűt és egy számot");
        
        break;
      }

      case "repeatedPassword": {
        validator.equals(signUpData.repeatedPassword, signUpData.password) ? 
          errorUpdate("repeatedPassword", "") : 
          errorUpdate("repeatedPassword", "A két jelszó nem egyezik meg");

        break;
      }

      case "agreed": {
        validator.equals(signUpData.agreed, true) ?
          errorUpdate("agreed", "") :
          errorUpdate("agreed", "El kell fogadnod a játék szabályait!");
      }
    }
  }

  const isValid = (obj) => {
    for (let key in obj) {
      if (obj[key] !== "") {
        return false;
      }
      return true;
    }
  }

  // TODO: onSubmit (connect the backend)
  const onSubmit = (event) => {
    event.preventDefault();

    if (isValid(errors)) {
      
    }
  }

  return (
    <div className="form register-form">
      <Text htmlTag="h1">Regisztráció</Text>

      <Input
        type="text"
        onChange={event => handleChange(event, "teamName")}
        value={signUpData.teamName}
        placeholder="Csapat név"
      />

      <Input
        type="text"
        onChange={event => handleChange(event, "location")}
        value={signUpData.location}
        placeholder="Helység"
      />

      <Input
        type="text"
        onChange={event => handleChange(event, "contact")}
        value={signUpData.contact}
        placeholder="Kapcsolattartó"
      />

      <Input
        type="email"
        onChange={event => handleChange(event, "email")}
        value={signUpData.email}
        placeholder="E-mail"
        validationLabel={errors.email}
        invalid={errors.email && "warning"}
      />

      <Input
        type="text"
        onChange={event => handleChange(event, "phoneNumber")}
        value={signUpData.phoneNumber}
        placeholder="Telefonszám"
        validationLabel={errors.phoneNumber}
        invalid={errors.phoneNumber && "warning"}
      />

      <Input
        type="password"
        onChange={event => handleChange(event, "password")}
        value={signUpData.password}
        placeholder="Jelszó"
        validationLabel={errors.password}
        invalid={errors.password && "warning"}
      />

      <Input
        type="password"
        onChange={event => handleChange(event, "repeatedPassword")}
        value={signUpData.repeatedPassword}
        placeholder="Jelszó ismét"
        validationLabel={errors.repeatedPassword}
        invalid={errors.repeatedPassword && "error"}
      />

      {/*TODO: href az anchornak*/}
      <CheckBox
        onChange={event => handleChangeCheckbox(event, "agreed")}
        isChecked={signUpData.agreed}
      >
        a <a>játék szabályzatát</a> elfogadom, és egyet értek azzal,
        hogy a megszegése esetén a kizárást kockáztatom:
      </CheckBox>

      <Button onClick={(event) => onSubmit(event)}>
        Regisztráció
      </Button>
    </div>
  );

}

export default RegisterForm
