import React from "react";

import Button from "../UI/Button"

const Navigation = (props) => {
  return (
    <div>
      <div className="nav main">
        <Button onClick={props.onClick} additionalClass="navigation">Fő oldal</Button>
      </div>
      <div className="nav rules">
        <Button onClick={props.onClick} additionalClass="navigation">Szabályzat</Button>
      </div>
      <div className="nav tasks">
        <Button onClick={props.onClick} additionalClass="navigation">Feladatok</Button>
      </div>
      <div className="nav tasks">
        <Button onClick={props.onClick} additionalClass="navigation">Állandó Feladatok</Button>
      </div>
      <div className="nav teams">
        <Button onClick={props.onClick} additionalClass="navigation">Csapatok</Button>
      </div>
      <div className="nav leaderboard">
        <Button onClick={props.onClick} additionalClass="navigation">Állás</Button>
      </div>
      <div className="nav contacts">
        <Button onClick={props.onClick} additionalClass="navigation">Elérhetőségek</Button>
      </div>
    </div>
  );
}

export default Navigation;
