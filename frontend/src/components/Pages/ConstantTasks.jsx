const ConstantTasks = () => {
  return (
    <div>
      <h1>Állandó Feladatok</h1>
      <p>
        Az idei Évek Egylete Vetélkedőn állandó feladatokkal igyekszünk még
        színesebbé tenni az egyleti életeteket. A következő feladatok
        megoldására a vetélkedő kezdetétől egészen a végéig (2022. november 27.)
        van lehetőségetek. Beküldésükre az utolsó havi feladattal egyidőben
        kerül majd sor. Igyekezzetek minél kreatívabban megoldani őket!
      </p>
      <h2>1. Portfólió:</h2>
      <p>
        Készítsetek egy portfóliót, amelyet a vetélkedő végén adtok le! A
        portfólióba minden hónapban legalább egy bejegyzést kell beiktatnotok.
        Minden hónap bejegyzését az aktuális hónaphoz kapcsolt, megadott
        időszakhoz kell igazítanotok. A havi bejegyzések témája és formája nem
        meghatározott, néhány ötletet adunk lejjebb. A portfólió formátuma nem
        meghatározott, felhasználható benne szöveg, kép és videó. A portfóliót
        az összes állandó feladattal együtt, novemberben kell leadnotok.
      </p>
      <ul>
        <li>május: '70-es évek</li>
        <li>június: '80-as évek</li>
        <li>július: '90-es évek </li>
        <li>augusztus: 2000-es évek</li>
        <li>szeptember: 2010-es évek</li>
        <li>október: jelen </li>
        <li>november: jövő</li>
      </ul>
      <p>Íme néhány ötlet, a bejegyzések tartalmához:</p>
      <ul>
        <li>az évtized legnépszerűbb szakmája</li>
        <li>receptek</li>
        <li>kollázs: moodboard és valós anyagok</li>
        <li>interjú: valakivel, aki az adott időszakban egyletezett.</li>
        <li>stb.</li>
      </ul>
      <h2>2. Egyleti alkalmakról készült fotók!</h2>
      <p>
        Készítsetek minél több fotót, az egyleti alkalmaitokról! Fontos: ennél a
        feladatnál a maximálisan pontozott fotók száma 50 db. Mindenképpen
        örülünk, ha ennél többet küldötök, de már nem tudjuk pontozni őket.
      </p>
      <p>
        A fotókat az összes állandó feladattal együtt, novemberben kell
        leadnotok.
      </p>
      <h2>3. Rendezvényen való részvétel.</h2>
      <p>
        Gyertek el minél több ODFIE-s rendezvényre! Részvételetekért értékes
        pontokat szereztek a vetélkedőn.
      </p>
      <h2>4. Social media jelenlét. Dokumentálás. #evekegylete </h2>
      <p>
        Készítsetek saját Instagram-oldalt és dokumentáljátok az Évek Egylete
        Vetélkedő feladatainak megoldását, történetekben vagy bejegyzésekben. Ne
        feledjétek megjelölni az @evekegylete Instagram-oldalt és a #evekegylete
        hashtaget!
      </p>
    </div>
  );
};

export default ConstantTasks;
