const Contacts = () => {
  // TODO: Beírni a kontaktokat
  return (
    <div className="div contacts">
      <h1 className="mb-4">Kapcsolattartók:</h1>
      <h2 style={{ fontSize: "20px" }}>Ilkei Árpád</h2>
      <p className="mb-4">
        Telefon: +40745938482<br></br> E-mail: ilkeiarpad02@gmail.com
      </p>
      <h2 style={{ fontSize: "20px" }}>Kolumbán Karola</h2>
      <p className="mb-4">
        Telefon: +40744112318<br></br> E-mail: kolumban.karola@gmail.com
      </p>
      <h2 style={{ fontSize: "20px" }}>Szilágyi Alpár</h2>
      <p className="mb-4">
        Telefon: +40749116862<br></br> E-mail: szilagyialpar09@gmail.com
      </p>
    </div>
  );
};

export default Contacts;
