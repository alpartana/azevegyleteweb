function Landing() {
  return (
    <div>
      <h1>Kedves Időutazók!</h1>
      <p>
        Magunk mögött hagyva az elmúlt évek nehézségeit, itt az idő megnyomni a
        Restart gombot. Viszont, hogy megértsük a jelenünket és a jövőben is
        méltó képviselői lehessünk az egykori egyletes értékeknek, fontos
        ismerni a múltat. Egy utazásra invitálunk benneteket a 70-es évektől
        kezdődően, hogy az évtizedekből inspirálódva építhessétek a jövőt. Az
        időutazó gép feltalálói keresik az arra alkalmas legénységet, akik
        kitartásukkal, együttműködésükkel, kreatívitásukkal és lelkesedésükkel
        bizonyítják, hogy érdemesek a jutalomra és akikre nyugodt szívvel rájuk
        bízhatják az Egylet sorsát. A visszaszámlálás hamarosan kezdődik,
        készüljetek, hisz bennetek a múlt, a jelen és a jövő!
      </p>
    </div>
  );
}

export default Landing;
