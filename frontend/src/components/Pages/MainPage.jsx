import Header from "../UI/Header";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import React from "react";
import LoginForm from "../Forms/LoginForm";
import Rules from "./Rules";
import Contacts from "./Contacts";
import Landing from "./Landing";
import LeaderBoard from "./LeaderBoard";
import Footer from "../UI/Footer";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Switch from "react-bootstrap/esm/Switch";
import Tasks from "./Tasks";
import Teams from "./Teams";
import ConstantTasks from "./ConstantTasks";

const MainPage = () => {
  // TODO: Beírni az ismertetőt
  return (
    <BrowserRouter
      style={{
        backgroundColor: "#503D2E",
      }}
    >
      <Container fluid style={{ height: "100%", width: "200vh" }}>
        <Row
          style={{
            backgroundColor: "#503D2E",
          }}
        >
          <Col id="header-col">
            <Header />
          </Col>
        </Row>
        <Row id="sb-menu-row" style={{ minHeight: "50vh" }}>
          <Col xs={1}></Col>
          <Col
            style={{
              backgroundColor: "#503D2E",
            }}
          >
            <Switch>
              <Routes>
                <Route path="/" element={<Landing />} />
                <Route path="/rules" element={<Rules />} />
                <Route path="/tasks" element={<Tasks />} />
                <Route path="constants" element={<ConstantTasks />} />
                <Route path="/teams" element={<Teams />} />
                <Route path="/leaderboard" element={<LeaderBoard />} />
                <Route path="/contacts" element={<Contacts />} />
              </Routes>
            </Switch>
          </Col>
          <Col xs={1}></Col>
          <Col xs={3}>{/* <LoginForm></LoginForm> */}</Col>
        </Row>
        <Row>
          <Footer></Footer>
        </Row>
      </Container>
    </BrowserRouter>
  );
};

export default MainPage;
