const Rules = () => {
  // TODO: Beírni a szabályokat
  return (
    <div>
      <h1>A játék szabályai</h1>
      <div>
        <h2>1. A játékról</h2>
        <ul>
          <li>
            egy településről egy egylet regisztrálhat, korlátlan létszámmal
          </li>
          <li>
            idén a csapatok 7 havi feladatra számíthatnak, amelyek megoldására a
            csapatoknak madjnem egy hónap áll rendelkezésére, a leadási
            határidőt a feladat megjelentetésével közöljük{" "}
          </li>
          <li>
            az elmúlt évekhez hasonlóan a csapatok rajtaütésszerű, meglepetés
            feladatokra is számíthatnak (random feladatok)
          </li>
          <li>
            különböző állandó feladatok futnak a vetélkedő teljes ideje alatt,
            melyeket az első feladattal egyidőben közlünk és a csapatoknak a
            vetélkedő zárásáig kell őket teljesíteniük
          </li>
          <li>
            a regisztrációval a csapatok felvállalják a játékszabályzat
            betartását, a zsűri pontozásának tiszteletben tartását
          </li>
          <li>
            a regisztrációval a csapatok kijelentik, hogy az adataik a
            valóságnak megfelelnek
          </li>
          <li>
            a regisztrációval a csapatok hozzájárulnak adataik felhasználásához
            és a weboldalon történő megjelentetéséhez
          </li>
          <li>
            a versenynek nincs résztvételi díja, a regisztráció teljesen
            ingyenes
          </li>
        </ul>
      </div>
      <div>
        <h2>2. Felelősségvállalás: </h2>
        <p>
          A weblapon szereplő tartalmak eredetiségéért és hitelességéért az
          Országos Dávid Ferenc Ifjúsági Egylet (továbbá ODFIE) nem vállal
          felelősséget a feltöltött tartalmakért a fájlokat feltöltő, megosztó
          személyek vállalják a felelősséget szerzői jogi probléma esetén
          lépjetek kapcsolatba az oldal üzemeltetőivel ( „Elérhetőségek”
          menüpont)
        </p>
      </div>
      <div>
        <h2>3. Felhasználási feltételek:</h2>
        <p>
          Az oldalon levő tartalmakra GNU licensz érvényes: a tartalmak szabadon
          felhasználhatók a forrás megjelölésével és a tartalom feltöltőinek
          megjelölésével
        </p>
        <p>
          Az Év Egylete weboldal az ODFIE tulajdonát képezi; bármilyen
          reprodukciója, másolása, más weblapokhoz való felhasználása szigorúan
          tilos a weblap nem rendeltetésszerű használata, a forráskód
          módosítása, az oldal törlése, működésének akadályozása a felhasználó
          IP-címének és számítógépnevének tiltását vonja maga után.
        </p>
        <p>
          Tilos az oldalon bántó, obszcén megjegyzéseket közzétenni, illetve
          egyesek számára sértő megoldásokat publikálni; a sértő dolgokat a
          verseny szervezői a feltöltő, közzétevő engedélye, előzetes
          beleegyezése nélkül törlik. Ha bebizonyosodik, hogy ezt valamelyik
          csapat követte el, a csapat büntethető, súlyosabb esetekben kitiltható
          a játékból.
        </p>
      </div>
      <div>
        <h2>4. Feladatok</h2>
        <ul>
          <li>
            {" "}
            a feladatok a következő adatokból állnak: feladat száma, címe,
            közzététel időpontja, a feladat lejáratának időpontja, leíró szöveg,
            csatolt fájlok, megoldás típusa
          </li>
          <li>
            {" "}
            az oldalon a feladatok a közzététel sorrendjében jelennek meg ( első
            helyen mindig az aktuális feladat áll és ezt követik kronológikusan,
            csökkenő sorrendben az előző feladatok)
          </li>
          <li> a feladatok a megadott napon, 23:59 órakor járnak le</li>
          <li>
            {" "}
            a feladatokkal kapcsolatos kérdéseiteket az “Elérhetőségek” fülön
            megjelölt kapcsolattartóknak, a megadott elérhetőségeken
            feltehetitek, ez a hivatalos kommunikációs csatorna
          </li>
          <li>
            {" "}
            minden feladat megoldását négy módon lehet elküldeni, a feladattól
            függően; egy feladatnál egy, több vagy akár az összes válasz típus
            használható, amit a szervezők határoznak meg:
          </li>
          <li> kép: JPG, PNG, GIF formátumú kép</li>
          <li>
            {" "}
            szöveges dokumentum: PDF, DOC, XLS (Microsoft Office Word, illetve
            Excel) dokumentum
          </li>
          <li> YouTube videó (videóhoz vezető link)</li>
          <li> szövegmező: a szövegmezőbe beírt válasz</li>
          <li> a feltöltött képek mérete nem haladhatja meg 5 MB-t </li>
        </ul>
      </div>
      <div>
        <h2>6. Megoldások feltöltése</h2>
        <p>
          A megoldások feltöltéséhez a csapatnak be kell jelentkeznie (vigyázat
          az ékezetekre, valamint a kis és nagy betűkre).{" "}
        </p>{" "}
        <p>
          {" "}
          Egy feladat feltöltött megoldásai utólag nem módosíthatók, nem
          kicserélhetők
        </p>
      </div>
      <div>
        <h2>7. Pontozás</h2>
        <h3>Általános információk</h3>
        <ul>
          <li>
            {" "}
            Minden beregisztrált csapat részére 10 bónusz pont kerül jóváírásra
            a regisztárciójáért.
          </li>{" "}
          <li>
            {" "}
            Amennyiben két vagy több csapat úgy dönt, hogy közösen oldanak meg
            egy feladatot, 10 bónuszpontban részesülnek, csapatonként. Közös
            feladatmegoldásra azonban a teljes vetélkedő során maximum
            háromszor, három különböző egylettel van lehetőség!
          </li>
        </ul>
        <h3>a. Havi feladatok pontozása</h3>
        <ul>
          <li>
            {" "}
            A pontozást a havi feladatok beküldési határidejétől számított egy
            héten belül közzétesszük.{" "}
          </li>
          <li>
            {" "}
            Egy havi feladatra a maximálisan elérhető pontszám 100 pont.{" "}
          </li>
          <li>
            {" "}
            Minden beküldött havi feladatra a beküldő hivatalból 10 pontot kap.{" "}
          </li>
          <li>
            {" "}
            Azokat a csapatokat, akik megszakítás nélkül küldik be folyamatosan
            a havi feladatok megoldását, egy streak-rendszer alapján
            jutalmazzuk. A bónusz pontok mennyisége hónapról-hónapra 10
            pontonként növekedik, megszakítás esetén ismét elölről kezdődik a
            folyamat.
          </li>
        </ul>
        <h3>b. Állandó feladatok pontozása</h3>
        <ul>
          <li>
            {" "}
            Az állandó feladatokkal elérhető maximális pontszámokat a feladatok
            közzétételekor megjelöljük.{" "}
          </li>
          <li>
            {" "}
            Az állandó feladatokkal megszerzett pontokat a vetélkedő zárásakor
            közöljük.
          </li>
        </ul>
        <h3>c. Random feladatok pontozása</h3>
        <ul>
          <li>
            {" "}
            A random feladatokkal elérhető maximális pontszámokat a feladatok
            közzétételekor megjelöljük.{" "}
          </li>
        </ul>
        <h2>8. Díjazás és nyeremények</h2>
        <ul>
          <li>
            {" "}
            A feladatok megoldásának értékelését egy előre meghatározott
            tagokból álló zsűri végzi.{" "}
          </li>
          <li>
            {" "}
            Az Évek Egylete Vetélkedő győztes csapata pénzjutalomban részesül és
            meghívást kap az ODFIE-gálára.{" "}
          </li>
          <li> A további helyezettek értékes nyereményekben részesülnek.</li>
        </ul>
      </div>
    </div>
  );
};

export default Rules;
