const Tasks = () => {
  // TODO: Megcsinálni backend után normálisan
  return (
    <div
      style={{
        justifyContent: "center",
        textAlign: "center",
      }}
    >
      <h1>Év Egylete - 1. feladat</h1>
      <p>Kedves Időutazók!</p>
      <p>
        A gépezet indulásra kész, de a Start gomb megnyomása előtt fontos, hogy
        megismerjük legénységünk tagjait. Ezért arra hívunk, hogy mutassátok be
        csapatotok „legjeit”. Ki tudja leginkább kezében tartani az irányítást?
        Ki tudja mindig feldobni a hangulatot vagy ki a legkreatívabb?
        Mindenféle képesség és tulajdonság hasznos lehet, tehát ne féljetek
        megmutatni, hogy kik vagytok, plusz pont, ha inspirálódtok a 70-es
        évekből!
      </p>
      <p>
        Bemutatkozásaitokat június 5-ig várjuk feltölteni az általatok választott
        formátumban, például fotó, bármilyen videóformátum, PPT prezentáció,
        Slam, stb. Engedjétek szabadjára a fantáziátokat és közben ne feledjétek
        el jól érezni magatokat!
      </p>
    </div>
  );
};

export default Tasks;
