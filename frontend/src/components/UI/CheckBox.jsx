import PropTypes from 'prop-types';

const CheckBox = (props) => {
  return (
    <div className={`checkbox ${props.additionalClass}`}>
      <div className="checkbox__container">
        <input 
          className="checkbox-styled" 
          type="checkbox"  
          onChange={props.onChange} 
          checked={props.isChecked} 
          disabled={props.isDisabled}
        />
      </div>
      <label>{props.children}</label>
    </div>
  );
};

CheckBox.propTypes = {
  children: PropTypes.node.isRequired,
  onChange: PropTypes.func.isRequired,
  isChecked: PropTypes.bool,
  isDisabled: PropTypes.bool,
};

CheckBox.defaultProps = {
  isChecked: false,
  isDisabled: false,
};

export default CheckBox;
