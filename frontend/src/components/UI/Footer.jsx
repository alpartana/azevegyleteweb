function Footer() {
  return (
    <footer className="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
      <div className="col-md-4 ml-10 d-flex align-items-center">
        <span className="text">Az Év Egylete Vetélkedő © 2022 ODFIE</span>
      </div>
      <ul className="nav col-md-4 justify-content-center list-unstyled d-flex">
        <li className="ms-3">
          <img src="odfie-logo.png" alt="odfie" style={{ height: "40px" }} />
        </li>
        <li className="ms-3">
          <img src="tamogatokmue.png" alt="mue" style={{ height: "40px" }} />
        </li>
        <li className="ms-3">
          <img src="tamogatokuufp.png" alt="uufp" style={{ height: "40px" }} />
        </li>
        <li className="ms-3 mt-3">
          <img src="tamogatokbga.png" alt="bga" style={{ height: "40px" }} />
        </li>
      </ul>
    </footer>
  );
}

export default Footer;
