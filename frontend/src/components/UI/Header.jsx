import { Link } from "react-router-dom";

function Header() {
  return (
    <div className="container">
      <header className="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
        <Link to="/">
          {" "}
          <img src="/logo.png" alt="logo" height="80" />
        </Link>
        <ul className="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
          <li>
            <Link
              to="/"
              className="headerbutton"
              style={{ backgroundColor: "XE3A72F" }}
            >
              Főoldal
            </Link>
          </li>
          <li>
            <Link to="/rules" className="headerbutton">
              Szabályzat
            </Link>
          </li>
          <li>
            <Link to="/tasks" className="headerbutton">
              Feladatok
            </Link>
          </li>
          <li>
            <Link to="/constants" className="headerbutton">
              Állandó Feladatok
            </Link>
          </li>
          <li>
            <Link to="teams" className="headerbutton">
              Csapatok
            </Link>
          </li>
          <li>
            <Link to="/leaderboard" className="headerbutton">
              Pontok
            </Link>
          </li>
          <li>
            <Link to="/contacts" className="headerbutton">
              Elérhetőségek
            </Link>
          </li>
        </ul>

        <div className="col-md-2 text-end"></div>
      </header>
    </div>
  );
}

export default Header;
