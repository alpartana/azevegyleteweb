export async function getTasks() {
  return fetch("192.168.0.216:3001/tasks", {
    method: "GET",
  })
    .then((res) => res.text())
    .then((response) => {
      if (response.ok) {
        return response;
      }
      console.log("Error occurred during getTasks.");
      throw new Error("Error occurred during  getTasks.");
    })
    .catch((error) => {
      console.log(error.message);
      console.log("Error occurred during fetch operation: ", error.message);
      throw new Error(error.message);
    });
}
